package com.example.todoleroy;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private ArrayList<String> arrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        ListView lv = findViewById(R.id.listView);
        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        arrayList = null;
        File file = new File(ContextCompat.getDataDir(this), "items.txt");
        if (file.exists()) {
            try {
                InputStream inputStream = new FileInputStream(file);
                Reader reader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(reader);
                String line;
                arrayList = new ArrayList<>();
                while ((line = bufferedReader.readLine()) != null) {
                    arrayList.add(line);
                }
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this, "Erreur lors de la lecture du fichier", Toast.LENGTH_SHORT).show();
            }
        }

        if (arrayList == null) {
            arrayList = new ArrayList<>(Arrays.asList("valeur01", "texte02", "valeur01", "texte04", "valeur05", "item06", "tâche07", "valeur08", "item09", "texte10",
                    "item11", "ligne12", "item13"));
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_checked, arrayList);
        lv.setAdapter(arrayAdapter);
    }

    ActivityResultLauncher<Intent> addItemLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == RESULT_OK) {
                    Intent data = result.getData();
                    if (data == null) {
                        return;
                    }
                    String item = data.getStringExtra("item");
                    //add item to list
                    ListView lv = findViewById(R.id.listView);
                    ArrayAdapter<String> arrayAdapter = (ArrayAdapter<String>) lv.getAdapter();
                    arrayList.add(item);
                    arrayAdapter.notifyDataSetChanged();
                }
            });

    public void buttonAddItem(View view) {

        //launch addItem activity
        Intent intent = new Intent(this, addItem.class);
        addItemLauncher.launch(intent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.deleteItems) {
            deleteItems(item);
            return true;
        } if (item.getItemId() == R.id.saveItems) {
            try {
                saveToFile(item);
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this, "Erreur lors de la sauvegarde", Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void saveToFile(MenuItem item) throws IOException {
        File file = new File(ContextCompat.getDataDir(this), "items.txt");
        file.createNewFile();
        ListView lv = findViewById(R.id.listView);
        ArrayAdapter<String> arrayAdapter = (ArrayAdapter<String>) lv.getAdapter();
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        for (int i = 0; i < arrayAdapter.getCount(); i++) {
            writer.write(arrayAdapter.getItem(i));
            writer.newLine();
        }
        writer.close();
        Toast.makeText(this, "Liste sauvegardée", Toast.LENGTH_SHORT).show();
    }



    private void deleteItems(MenuItem item) {
        ListView lv = findViewById(R.id.listView);
        ArrayAdapter<String> arrayAdapter = (ArrayAdapter<String>) lv.getAdapter();
        for (int i = arrayAdapter.getCount() - 1; i >= 0; i--) {
            if (lv.isItemChecked(i)) {
                arrayList.remove(i);
                arrayAdapter.notifyDataSetChanged();
            }
        }
        lv.clearChoices();
    }


}