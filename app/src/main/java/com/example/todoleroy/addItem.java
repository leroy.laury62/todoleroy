package com.example.todoleroy;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class addItem extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
    }

    public void cancel(View view) {
        finish();
    }

    public void saveItem(View view) {
        EditText editTextItem = findViewById(R.id.editTextItem);
        String item = editTextItem.getText().toString();

        //return item to MainActivity
        if (item.isEmpty()) {
            return;
        }
        getIntent().putExtra("item", item);
        setResult(RESULT_OK, getIntent());
        finish();
    }
}